package com.devcamp.customervisit_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customervisit_api.model.Customer;
@Service
public class CustomerService {
    Customer customer1 = new Customer("Nguyen Van A");
    Customer customer2 = new Customer("Nguyen Van B");
    Customer customer3 = new Customer("Nguyen Van C");

    public ArrayList<Customer> getAllCustomer(){
        ArrayList<Customer> lCustomers = new ArrayList<>();
        lCustomers.add(customer1);
        lCustomers.add(customer2);
        lCustomers.add(customer3);
        return lCustomers;
    }

    public Customer getCustomer1() {
        return customer1;
    }

    public Customer getCustomer2() {
        return customer2;
    }

    public Customer getCustomer3() {
        return customer3;
    }
}
