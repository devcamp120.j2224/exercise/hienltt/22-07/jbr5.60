package com.devcamp.customervisit_api.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.customervisit_api.model.Visit;
@Service
public class VisitService {
    CustomerService customerService = new CustomerService();
    Visit visit1 = new Visit(customerService.getCustomer1(), new Date());
    Visit visit2 = new Visit(customerService.getCustomer2(), new Date());
    Visit visit3 = new Visit(customerService.getCustomer3(), new Date());

    public ArrayList<Visit> getAllVisit(){
        ArrayList<Visit> listVisit = new ArrayList<>();
        listVisit.add(visit1);
        listVisit.add(visit2);
        listVisit.add(visit3);
        return listVisit;
    }
}
