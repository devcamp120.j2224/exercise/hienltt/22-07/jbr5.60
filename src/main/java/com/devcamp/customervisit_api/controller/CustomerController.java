package com.devcamp.customervisit_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisit_api.model.Customer;
import com.devcamp.customervisit_api.service.CustomerService;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @CrossOrigin
    @GetMapping("/customers")
    public ArrayList<Customer> getListCustomer(){
        ArrayList<Customer> listCustomer = customerService.getAllCustomer();

        return listCustomer;
    }
}
