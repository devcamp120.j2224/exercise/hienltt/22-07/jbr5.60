package com.devcamp.customervisit_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisit_api.model.Visit;
import com.devcamp.customervisit_api.service.VisitService;

@RestController
public class VisitController {
    @Autowired
    VisitService visitService;

    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> getListVisit(){
        ArrayList<Visit> lVisits = visitService.getAllVisit();

        return lVisits;
    }
}
